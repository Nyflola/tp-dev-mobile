import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import InformationComponent from './components/InformationComponent';
import HomeScreen from './components/HomeScreen';
import SearchScreen from './components/SearchScreen';

function HomeView({ navigation }) {
  return (
    <HomeScreen/>
  );
}

function SearchView({ navigation }) {
  return (
    <SearchScreen/>
  );
}

function InfoScreen({ navigation }) {
  return (
    <InformationComponent/>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeView} />
        <Tab.Screen name="Search" component={SearchScreen} />
        <Tab.Screen name="Info" component={InfoScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

import fetch from 'node-fetch';
const URL_API_base='https://maps.googleapis.com/maps/api/place/textsearch/json?query='
const rad='&radius=2000&'
const KEY='key=AIzaSyBdrMM6NpzCgPhI5A9kcBN0t1p-l6BqQQo'
const URL_Detail='https://maps.googleapis.com/maps/api/place/details/json?place_id='
class GmapApi{
    constructor(type,location,id){
        this.type=type;
        this.location=location;
        this.id=id;
    }

    getUser(){
        var List_Place=[];
        return new Promise( (resolve,reject) => {
            fetch(URL_API_base+this.type+"&location="+this.location+rad+KEY).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    let Place = apiResponse
                
                    let PL2=Place.results
                    for(let elmt in PL2){
                            
                    let place = {
                        place_id:`${Place.results[elmt].place_id}`,
                        name: `${Place.results[elmt].name}`,
                        formatted_address:`${Place.results[elmt].formatted_address}`
                        
                    }
                    List_Place.push(place);
                   
                    }
                    
                    resolve(List_Place)
                    
                })
            })
        })
    }
    
    getDetails(){
        return new Promise( (resolve,reject) => {
            fetch(URL_Detail+this.id+'&fields=formatted_phone_number&'+KEY).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    let DetailPlace = apiResponse.result
                    let Detail_Places= DetailPlace.formatted_phone_number
                   
                    resolve(Detail_Places)
                    
                })
            })
        })


    }

    

}
export default GmapApi



import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView, StatusBar, Image } from 'react-native';
import TitleComponents from './TitleComponents';
import { Title,Button,Avatar, Card, Paragraph } from 'react-native-paper';
import {CardComponent, Box} from './CardComponent';

class HomeScreen extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            pyschologue : false
        }
    }

    render(){
        return (
            <ScrollView style={{backgroundColor : "#CBCBD1"}}>
                <View style={styles.header}>
                    <TitleComponents />
                </View>
                <View>
                    <Box description={'L’application qui vous aide pendant le COVID-19.'+
                        '\n\nL’onglet “Rechercher” vous permet de rechercher un psychologue ou un psychiatre'+
                        'près de chez vous pour vous permettre de prendre un rendez-vous en seulement quelques clics.'+
                        '\n\nL’onglet “Informations” vous permet de trouver les numéros et informations importantes'}
                    />
                    <CardComponent
                    buttonText= "Qu'est-ce qu'un pyschologue ?" 
                    description={'\tLe psychologue est le professionnel formé à l’analyse et la compréhension'+ 
                    'de la psychologie humaine, ainsi que des facteurs (internes ou externes à l’individu)'+ 
                    'qui influencent.De même qu’un ingénieur analyse une situation ou un système et recherche' +
                    'ensuite des solutions, un psychologue analyse un fonctionnement et propose des moyens pour' +
                    'trouver les solutions. \n\n'+
                    'Cela concerne le fonctionnement des :\n'+
                    '\t- pensées (niveau cognitif) (ce qui a trait aux représentations mentales, aux pensées) ;\n'+
                    '\t- émotions et sentiments (niveau affectif) ;\n'+
                    '\t- comportements (niveau comportemental)  ;\n'+
                    '\t- contextuels (niveau social, familial, professionnel, etc.).\n' }
                    />
                    <CardComponent
                    buttonText= "Qu'est-ce qu'un psychiatre ?" 
                    description={'\tUn psychiatre est un médecin spécialiste, au même titre qu’un cardiologue'+
                    'ou un chirurgien.\n\n\tDonc, en plus de sa formation médicale générale, il a une '+
                    'formation supplémentaire de cinq ou six ans pendant laquelle il se concentrera '+
                    'sur les maladies mentales. Le psychiatre est le seul spécialiste des maladies '+
                    'mentales qui peut poser un diagnostic basé sur une évaluation complète incluant '+
                    'un examen mental et physique, des analyses de laboratoire, de l’imagerie médicale '+
                    'et une histoire psychosociale détaillée.\n\n\t Une fois le diagnostic posé, le psychiatre '+
                    'peut traiter lui-même la maladie mentale ou coordonner le traitement pour son patient. ' +
                    '\nDans son coffre à outils, il dispose de différentes psychothérapies, d’un éventail de ' +
                    'médicaments, des techniques de neurostimulation et des interventions sociales.'} 
                    />
                    <CardComponent
                    buttonText= "Qu'est-ce qu'un psychanalyste ?" 
                    description={'\tLe psychanalyste désigne un thérapeute dont l’objectif est de traiter ses patients en '+
                    'utilisant la méthode psychanalytique inventée par Sigmund Freud au début du 20e siècle.\n\n'+
                    '\tCette pratique psychothérapeutique vise à soulager les patients par l’exploration de leur '+
                    'inconscient afin de les aider à résoudre leurs difficultés psychologiques.\n\n\tC’est au '+
                    'travers d’une écoute attentive de la parole que le psychanalyste propose une '+
                    'interprétation et amène son patient à effectuer un travail sur lui-même.' +
                    '\n\n\tLes psychanalystes n’ont pas de diplôme reconnu officiellement mais beaucoup d’entre eux '+
                    'sont soit psychiatres, soit psychologues. Plus rarement, il peut s’agir de philosophes ou de ' +
                    'linguistes par exemple.'}
                    /> 
                </View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        flex: 1
    },
    body: {

    },
    logo: {
        height: 50,
        width:100,
    }
});

export default HomeScreen;

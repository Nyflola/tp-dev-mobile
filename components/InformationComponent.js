import React from 'react';
import { Button, StyleSheet, Text, View,ScrollView } from 'react-native';
import {CardComponent, Box} from './CardComponent';
class InformationComponent extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        const { navigation } = this.props;
        return (
            <ScrollView style={{backgroundColor : "#CBCBD1"}}>
            <View >
                 <Box description={'Car votre santé mentale est aussi importante que votre santé physique, vous pouvez trouver ci-dessous des numéros d aide gratuits'
                        }
                    />
                    <CardComponent
                    buttonText= "Soutien psychologique (tous publics) " 
                    description={"Les dispositifs apparaissent par ordre alphabétique.\n\
                    \n\
                        Croix-Rouge écoute\n\
                        Service de soutien par téléphone (solitude, dépression, violence, addictions, etc.)\n\
                        0 800 858 858 (lundi au vendredi 9h à 19h, samedi dimanche 12h à 18h)\n\
                        \n\
                        Empreintes, accompagner le deuil\n\
                        Ligne d’écoute pour les personnes endeuillées\n\
                        01 42 38 08 08 (lundi au vendredi 10h-13h / 14h30-17h30, et jusqu’à 19h30 le mardi)\n\
                        \n\
                        Mieux traverser le deuil\n\
                        Plateforme d’orientation pour les personnes endeuillées\n\
                        tchat en cliquant sur l’icône en page d’accueil en bas à gauche (7j/7 et 24h/24)"}
                    />
                    <CardComponent
                    buttonText= "Soutien psychologique (publics ciblés)" 
                    description={"Agri’Ecoute\n\
                    Ligne d’écoute, à destination des agriculteurs en souffrance psychologique et leur entourage\n\
                    09 69 39 29 19  (7j/7 et 24h/24)\n\
                    \n\
                    Aide aux victimes\n\
                    Informations et écoute par des professionnels pour les victimes d’agression, vol, harcèlement, accident de la circulation, catastrophe naturelle, etc.\n\
                    116 006 (7j/7, 9h-19h) ou par mail  : victimes@france-victimes.fr\n\
                    \n\
                    Autisme info Service\n\
                    Dispositif gratuit et national d’écoute, aide, information et orientation par téléphone, mail et chat, pour les personnes avec autisme, leur entourage et les professionnels intervenant à leurs côtés\n\
                    0 800 71 40 40 (lundi au vendredi 9h-13h + mardi 18h-20h)\n\
                    \n\
                    Avec nos proches\n\
                    Ligne nationale des aidants, tenue par une trentaine d’écoutants bénévoles\n\
                    01 84 72 94 72 (7j/7 8h-22h)\n\
                    \n\
                    Cellule de soutien psychologique pour les chefs d’entreprise\n\
                    Soutien, information et orientation des entrepreneurs en détresse en cette période de crise économique et sanitaire (avec l’Association APESA, Aide psychologique aux entrepreneurs en souffrance aiguë)\n\
                    0805 65 50 50 (7j/7 8h-20h)"} 
                    />
                    <CardComponent
                    buttonText= "Addictions" 
                    description={"Alcool Info Service\n\
                    Information, soutien, conseil et orientation pour les personnes en difficulté avec l’alcool, et pour leurs proches\n\
                    0 980 980 930 (7j/7 8h-2h)\n\
                    \n\
                    Drogue info service\n\
                    Information, soutien, conseil et orientation pour les personnes en difficulté avec l’usage de drogues, et pour leurs proches\n\
                    0 800 23 13 13 (7j/7 8h-2h)\n\
                    \n\
                    Écoute Cannabis\n\
                    Information, soutien, conseil et orientation pour les personnes en difficulté avec l’usage de drogues, et pour leurs proches\n\
                    0 980 980 940 ((7j/7 8h-2h)"}
                    />
                    <CardComponent
                    buttonText= "Troubles psychiques" 
                    description={"Argos2001\n\
                    Aide aux personnes souffrant de troubles bipolaires et à leurs proches\n\
                    01 46 28 01 03 (lundi au vendredi 10h-13h)\n\
                    \n\
                    France dépression\n\
                    Aide aux personnes souffrant de troubles dépressifs ou bipolaires et à leurs proches\n\
                    07 84 96 88 28 (lundi au vendredi 14h-19h00)\n\
                    \n\
                    Écoute Famille Unafam\n\
                    Information, orientation et soutien psychologique de personnes confrontées aux troubles psychiques d’un proche\n\
                    01 42 63 03 03 (lundi au vendredi 9h-13h et 14h-18h)"}
                    />
                     
            </View> 
            </ScrollView>
        )
    }
}



export default InformationComponent
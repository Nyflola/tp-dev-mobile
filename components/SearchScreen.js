import React from 'react';
import { StyleSheet,  View, FlatList } from 'react-native';
import GmapApi from '../services/GmapApi';
import ViewComponent from './ViewComponent';
import Constants from 'expo-constants';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import RNPickerSelect from 'react-native-picker-select';

const GOOGLE_PLACES_API_KEY = 'AIzaSyBdrMM6NpzCgPhI5A9kcBN0t1p-l6BqQQo'; 
const URL_Detail='https://maps.googleapis.com/maps/api/place/details/json?place_id='
class SearchScreen extends React.Component {
    
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            usersData: [],
            type:""
        }
        

        
    }
        
   proxyStatelessComponent( props ){
        return (
            <ViewComponent user={props.item} />
        )
    }

    display(type,local){
        this.setState({usersData:[]});
        
        let usersFetch = new GmapApi(type,local,"")
        usersFetch.getUser().then(user => {
            this.state.usersData.push(user);
            this.setState({
                usersData: this.state.usersData.[0],
                
            });
            

        }) 
       
        
    };

    getDetails(donne){
        return new Promise( (resolve,reject) => {
            fetch(URL_Detail+donne+'&key='+GOOGLE_PLACES_API_KEY).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    let DetailPlace = apiResponse.result
                    let lat=DetailPlace.geometry.location.lat;
                    let long=DetailPlace.geometry.location.lng;
                    let coord=lat.toString()+','+long.toString()
                    resolve(coord)
                    
                })
            })
        })
      
      }

    async coord(donne){
       const result= await this.getDetails(donne)
      this.display(this.state.type,result)
      
   }
   
    
 
    


 render() {
  return (
    <View>
       
          
              <RNPickerSelect
              style={styles.item}
                  onValueChange={(value) => this.setState({type:value})}
                  items={[
                      { label: 'Psychologue', value: 'psychologue' },
                      { label: 'Psychiatre', value: 'psychiatre' },
                      { label: 'Psychanalyste', value: 'psychanalyste' },
                      { label: 'Médecin', value: 'medecin' },
                  ]}
              />
          <View style={styles.container}>
            <GooglePlacesAutocomplete
              placeholder="Search"
              query={{
                key: GOOGLE_PLACES_API_KEY,
                language: 'fr', // language of the results
              }}
              listViewDisplayed="auto" 
              onPress={(data, details = null) => (this.coord(data.place_id))}
              onFail={(error) => console.error(error)}
              requestUrl={{
                url:
                  'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                useOnPlatform: 'web',
              }} 
            />
        </View>  
   
          <FlatList
            
            style={styles.flatlist}
            data={this.state.usersData}
            renderItem={this.proxyStatelessComponent.bind(this)}
            keyExtractor={(item, id) => id.toString()}            
          />
      
  </View>
  );
};
  

    
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
    paddingTop: Constants.statusBarHeight + 10,
    backgroundColor: '#3C3C78',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    
  },
  header : {
    padding: 10,
    paddingTop: Constants.statusBarHeight + 10,
    backgroundColor: '#3C3C78',
    
  },
  list : {
    flex : 9
  }
  ,
  flatlist: {
    backgroundColor : '#CBCBD1',
  }
});
export default SearchScreen;
import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import GmapApi from '../services/GmapApi';
import { Avatar, Card, Title, Paragraph } from 'react-native-paper';

class ViewComponent extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            phoneNumber:"0",
        }

        this.display()
    }
    
    display(){
        let usersFetch = new GmapApi("","",this.props.user.place_id)
        usersFetch.getDetails().then(phone => {
            
            this.setState({
                phoneNumber: phone
                
            });
        
        })
        
    };
   
    render(){
        
        return (
            <View style={styles.card} >
                    <Card style={styles.title}>
                        <Paragraph style={{color:'#EAEAEC',marginLeft:'1%'}}>{this.props.user.name}</Paragraph>
                    </Card>
                    <Card>
                        <Paragraph style={{marginLeft:'1%'}}>{this.props.user.formatted_address + '\nTéléphone : ' +this.state.phoneNumber}</Paragraph>
                    </Card>
                    
            </View>
        )
    }
    }

const styles = StyleSheet.create({
    card: {
        backgroundColor : "#EAEAEC",
        padding : 10,
    },
    title :{
        backgroundColor : '#3C3C78',
        
    }
});


export default ViewComponent

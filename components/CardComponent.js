import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Title,Button,Avatar, Card, Paragraph } from 'react-native-paper';

class CardComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            show : false
        }
    }
    componentHideAndShow = () => {
        this.setState(previousState => ({ show: !previousState.show }))
      }
    render(){
        return (
            <View>
                <View style={styles.button}>
                    <Button color="#3C3C78" mode="contained" onPress={this.componentHideAndShow}>
                        {this.props.buttonText}
                    </Button>
                </View>
                {this.state.show ? <Box description={this.props.description}/> : null}
            </View>
        )
    }
}
class Box extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return(
            <Card style={styles.card}>
                <Card.Content>
                    <Paragraph>
                        {this.props.description}
                    </Paragraph>
                </Card.Content>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      fontSize: 30
    },
    button: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom : 10,
        marginTop : 10,
    },
    card: {
        backgroundColor : "#EAEAEC"
    }
});

export {CardComponent, Box};

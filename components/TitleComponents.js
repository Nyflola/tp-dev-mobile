import * as React from 'react';
import { StyleSheet, Text, View,Image} from 'react-native';
import { Title,Button,Avatar, Card, Paragraph } from 'react-native-paper';

class TitleComponents extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            
        }
    }

    render(){
        return (
            <View>
                <Image
                        style={styles.title}
                        source={require('../assets/Logo.png')}
                    />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    title: {
      height: 200,
      width: 400,
      alignSelf : "center",
    },
});

export default TitleComponents;